import random

N = 10000

def sim(n):
    doors = ['goat1', 'goat2', 'car']
    wins_staying = 0
    wins_changing = 0

    for i in range(n):
        random.shuffle(doors)

        choice = random.choice(doors)

        while 1:
            opened_door = random.choice(doors)
            if 'goat' in opened_door and opened_door != choice:
                break

        while 1:
            choice_2nd = random.choice(doors)
            if choice_2nd != choice and choice_2nd != opened_door:
                break

        if choice == 'car':
            wins_staying += 1

        if choice_2nd == 'car':
            wins_changing += 1

    return wins_staying / n, wins_changing / n

def main():
    win_rate_staying, win_rate_changing = sim(N)
    print(win_rate_staying, win_rate_changing)

if __name__ == '__main__':
    main()
