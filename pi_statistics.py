import statistics
import pi_sim

N = 10000

def main():
    pi_list = []

    for i in range(N):
        pi = pi_sim.sim(N)
        pi_list.append(pi)

    print('mean={}'.format(statistics.mean(pi_list)))
    print('pvariance={}'.format(statistics.pvariance(pi_list)))
    print('pstdev={}'.format(statistics.pstdev(pi_list)))

if __name__ == '__main__':
    main()
