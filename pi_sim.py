import random
import math

N = 10000

def x():
    return random.uniform(0, 1) # [0, 1]

def y():
    return random.uniform(0, 1) # [0, 1]

def sim(n):
    score = 0

    for i in range(n):
        distance = math.sqrt((x() ** 2) + (y() ** 2))

        if distance <= 1:
            score += 1

    return (4 * score) / n

def main():
    pi = sim(N)
    print(pi)

if __name__ == '__main__':
    main()
