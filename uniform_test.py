import matplotlib.pyplot as plt
import pi_sim

N = 10000

def main():
    u = [pi_sim.x() for i in range(N)]
    plt.hist(u)
    plt.title('n={}'.format(N))
    plt.show()

if __name__ == '__main__':
    main()
